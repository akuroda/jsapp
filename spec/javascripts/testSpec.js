describe("test1", function() {
    it("should return x + y", function() {
        expect(addFunc("aaa", "bbb")).toEqual("aaabbb");
    });

    var json = {
        "country":{"Egypt":"0.0052014","Algeria":"0.0051878","Morocco":"0.0051381","US":"0.0051223","Iran":"0.0051208"},
        "sex":{"M":"0.509","F":"0.491"}
    }

    it("should add table", function() {
        loadFixtures("table.html");
        expect($(".tbl_s_body")).toExist();
        expect($(".tbl_s_body tr")).not.toExist();
        expect($(".tbl_c_body")).toExist();
        expect($(".tbl_c_body tr")).not.toExist();
        drawTable(json);
        var s = expect($(".tbl_s_body"));
        s.toContainText("M");
        s.toContainText("0.509");
        s.toContainText("F");
        s.toContainText("0.491");

        var c = expect($(".tbl_c_body"));
        c.toContainHtml("<tr><td>Egypt</td><td>0.0052014</td></tr>");
        c.toContainHtml("<tr><td>Algeria</td><td>0.0051878</td></tr>");
        c.toContainHtml("<tr><td>Morocco</td><td>0.0051381</td></tr>");
        c.toContainHtml("<tr><td>US</td><td>0.0051223</td></tr>");
        c.toContainHtml("<tr><td>Iran</td><td>0.0051208</td></tr>");
    });

});
